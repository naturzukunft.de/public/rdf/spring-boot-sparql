This sample is based on the [QueryResponder of JervenBolleman](https://github.com/eclipse/rdf4j/pull/2905).   
My Problem with the QueryResponder is, that in our project we build seperation of concerns by maven modules. 

- **Controller**   
  Contains the HTTP Stuff  

- **Domain**  
  Contains the BusinessLogic  

- **Infrastructure**  
  Adapts different Services (Microservice, Database, etc. etc.)  

More details about the ideas can be found [here](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html)

So I disassembled the QueryResponder a bit, below is a diagram showing the maven modules and classes.

![overview](doc/overview.png "overview")

# Test
* git clone https://gitlab.com/naturzukunft.de/public/rdf/spring-boot-sparql.git
* cd spring-boot-sparql//spring-boot-sparql-controller/
* mvn spring-boot:run
* Change to new terminal
* curl -H 'accept: application/json' "http://localhost:8080/actors/0815/sparql?query=SELECT+*+WHERE+%7B+%3Fs+%3Fp+%3Fo+%7D"
