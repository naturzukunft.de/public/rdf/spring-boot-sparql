package de.naturzukunft.rdf4j.sparql.spring.infrastructure;

import java.io.IOException;

import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.config.RepositoryConfig;
import org.eclipse.rdf4j.repository.config.RepositoryImplConfig;
import org.eclipse.rdf4j.repository.manager.RepositoryManager;
import org.eclipse.rdf4j.repository.sail.config.SailRepositoryConfig;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.eclipse.rdf4j.sail.nativerdf.config.NativeStoreConfig;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
class ActorRepositoryManagerSimple implements ActorRepositoryManager {

	private RepositoryManager repositoryManager;
	
	public ActorRepositoryManagerSimple(RepositoryManager repositoryManager) {
		this.repositoryManager = repositoryManager;
	}

	@Override
	public Repository getRepository(String actorId) {
		Repository repository = repositoryManager.getRepository(actorId);
		if(repository==null) {
			repository = createRepo(actorId);
		}
		return repository;
	}
	
	private Repository createRepo(String repositoryId) {
		RepositoryImplConfig repositoryTypeSpec = new SailRepositoryConfig(new NativeStoreConfig());
		repositoryManager.addRepositoryConfig(new RepositoryConfig(repositoryId, repositoryTypeSpec));
		return  repositoryManager.getRepository(repositoryId);
	}
	
	@EventListener(ApplicationReadyEvent.class)
	public void initRepoWithDummyData() {
		Repository repository = getRepository("0815");
		try(RepositoryConnection con = repository.getConnection()) {
			con.add(new ClassPathResource("ComplexSample.ttl").getInputStream(), RDFFormat.TURTLE);
		} catch (RDFParseException | RepositoryException | IOException e) {
			throw new RuntimeException("unable to import dummy data", e);
		}
	}
}
