package de.naturzukunft.rdf4j.sparql.spring.domain;

import java.io.OutputStream;

public interface SparqlQueryEvaluator {
	void evaluate(String actorId, String query, String acceptHeader, String defaultGraphUri,
			String namedGraphUri, OutputStream outputStream );
}