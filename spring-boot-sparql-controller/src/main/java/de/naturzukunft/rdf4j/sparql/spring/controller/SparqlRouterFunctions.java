package de.naturzukunft.rdf4j.sparql.spring.controller;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Configuration
	public class SparqlRouterFunctions {
		
	private final SparqlController sparqlController;
	
	@Bean
	public RouterFunction<?> sparqlRouterFunction() {
		return RouterFunctions.route(RequestPredicates.GET("/actors/{actorId}/sparql"), sparqlController::sparqlGet);
	}
}
